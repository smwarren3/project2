% S. Matthew Warren
% EOC 6189 Project 2
% Upwinding
% Due 5 March 2019

% Second Order

L = 1;
dt = 10^-4;
tEnd = 2;

dx = 1/1000; 
x = 0:dx:L;
t = 0:dt:tEnd;
Phi = zeros([length(x) length(t)]);
% u = 1 for Part B, -1 for Part D, 2sin(2pi(t)) for Part E
u = ones([length(x) length(t)]).*-1; 
u = u(:,1).*2.*sin(2.*pi.*t);

% IC:
Phi(:,1) = exp(-(((x-0.5).^2)./0.01));


for n=1:length(t)-1
    for i = 1:length(x)
        if u(i,n) <= 0
            if i == 1
                Phi(i,n+1)=((u(i,n).*((3.*Phi(i,n)-4.*Phi(length(x),n)+Phi(length(x)-1,n))/(2.*dx))).*dt)+Phi(i,n);
            elseif i == 2
                 Phi(i,n+1)=((u(i,n).*((3.*Phi(i,n)-4.*Phi(i-1,n)+Phi(length(x),n))/(2.*dx))).*dt)+Phi(i,n);    
            else   
                Phi(i,n+1)=((u(i,n).*((3.*Phi(i,n)-4.*Phi(i-1,n)+Phi(i-2,n))/(2.*dx))).*dt)+Phi(i,n);
            end
        else
            if i == length(x)
                Phi(i,n+1)=((u(i,n).*((-3.*Phi(i,n)+4.*Phi(1,n)-Phi(2,n))/(2.*dx))).*dt)+Phi(i,n);
            elseif i == length(x)-1
                 Phi(i,n+1)=((u(i,n).*((-3.*Phi(i,n)+4.*Phi(i+1,n)-Phi(1,n))/(2.*dx))).*dt)+Phi(i,n);    
            else   
                Phi(i,n+1)=((u(i,n).*((-3.*Phi(i,n)+4.*Phi(i+1,n)-Phi(i+2,n))/(2.*dx))).*dt)+Phi(i,n);
            end
        end
    end
end

%% 101 Points

dx100 = 1/100; 
x100 = 0:dx100:L;
Phi100 = zeros([length(x100) length(t)]);

% IC:
Phi100(:,1) = exp(-(((x100-0.5).^2)./0.01));

for n=1:length(t)-1
    for i = 1:length(x100)
        if u(i,n) <= 0
            if i == 1
                Phi100(i,n+1)=((u(i,n).*((3.*Phi100(i,n)-4.*Phi100(length(x100),n)+Phi100(length(x100)-1,n))/(2.*dx100))).*dt)+Phi100(i,n);
            elseif i == 2
                 Phi100(i,n+1)=((u(i,n).*((3.*Phi100(i,n)-4.*Phi100(i-1,n)+Phi100(length(x100),n))/(2.*dx100))).*dt)+Phi100(i,n);    
            else   
                Phi100(i,n+1)=((u(i,n).*((3.*Phi100(i,n)-4.*Phi100(i-1,n)+Phi100(i-2,n))/(2.*dx100))).*dt)+Phi100(i,n);
            end
        else
            if i == length(x100)
                Phi100(i,n+1)=((u(i,n).*((-3.*Phi100(i,n)+4.*Phi100(1,n)-Phi100(2,n))/(2.*dx100))).*dt)+Phi100(i,n);
            elseif i == length(x100)-1
                 Phi100(i,n+1)=((u(i,n).*((-3.*Phi100(i,n)+4.*Phi100(i+1,n)-Phi100(1,n))/(2.*dx100))).*dt)+Phi100(i,n);    
            else   
                Phi100(i,n+1)=((u(i,n).*((-3.*Phi100(i,n)+4.*Phi100(i+1,n)-Phi100(i+2,n))/(2.*dx100))).*dt)+Phi100(i,n);
            end
        end
    end
end
%%

figure(2)
plot(Phi(:,1)); 
hold on; 
plot(PhiO1u1(:,length(t)));
plot(PhiO2u1(:,length(t)));
plot(PhiO1un1(:,length(t)));
plot(PhiO2un1(:,length(t)));
plot(PhiO1pi(:,length(t)));
plot(PhiO2pi(:,length(t)));
axis 'tight'
legend('Initial', '1st Order u=1', '2nd order u=1',...
    '1st order u=-1', '2nd order u=-1','1st Order u=2sin(2*pi*t)',...
    '2nd Order u=2sin(2*pi*t)','Location', 'northwest')
ylabel('Phi')
xlabel('x')
title('Initial and final comparison, 1000 sections')

%% Video
figure(1)
vidObj = VideoWriter('2nd Order u=2(sin*pi*t).mp4', 'MPEG-4');
open(vidObj);
h = animatedline('MaximumNumPoints', length(x100), 'Color', 'b',...
    'LineWidth', 2, 'LineStyle', '--');
e = animatedline('MaximumNumPoints', length(x), 'Color', 'r');
axis([0 1 0 1.05])
legend('100 Sections', '1000 Sections');
xlabel('x')
ylabel('Phi')
title('2nd Order dx comparison')
 a = tic;
for k = 1:length(t)
    addpoints(e,x,Phi(:,k));
    addpoints(h,x100,Phi100(:,k));
        b = toc(a); % check timer
    if b > (1/50) % Slow down the drawing speed by a factor in the denominator
        drawnow
         M(k) = getframe(gcf);
         writeVideo(vidObj,M(k));
         a = tic; % reset timer after updating
    end
end
close(vidObj);